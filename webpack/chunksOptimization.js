const chunksOptimization = {
	runtimeChunk: 'single',
	moduleIds: 'hashed',
	splitChunks: {
		chunks: 'all',
		maxInitialRequests: Infinity,
		minSize: 30000,
		cacheGroups: {
			vendor: {
				test: /[\\/]node_modules[\\/]/,
				name(module) {
					const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
					return `npm.${packageName.replace('@', '')}`;
				},
			},
		},
	},
};

module.exports = chunksOptimization;
