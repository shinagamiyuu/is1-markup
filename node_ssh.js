/* eslint-disable max-len */
const fs = require('fs');
const childProcess = require('child_process');
const path = require('path');
const NodeSsh = require('node-ssh');
const chalk = require('chalk');
const untildify = require('untildify');
const boxen = require('boxen');

const ssh = new NodeSsh();

const logErr = chalk.bgRed.bold.whiteBright;

const sshPath = untildify('~/.ssh/id_rsa');

/* НАЗВАНИЕ ПАПКИ САЙТА НА СЕРВЕРЕ */
//const PROJECT_DIRECTORY = 'frontend';
const PROJECT_DIRECTORY = undefined;

if (typeof PROJECT_DIRECTORY === 'undefined') {
	console.log(logErr('ЗАДАЙТЕ НАЗВАНИЕ ПЕРЕМЕННОЙ PROJECT_DIRECTORY в файле node_ssh.js'));
	process.exit(1);
}

const pwd = process.env.PWD;
const remoteDir = `/var/www/fmake/data/www/front-end.fmake.ru/${PROJECT_DIRECTORY}`;
const markupBranch = 'markup';

childProcess.exec('git rev-parse --abbrev-ref HEAD', (err, stdout) => {
	if (err) {
		return console.log('ERROR:', err);
	}

	console.log(chalk.blue(`Current branch is: ${chalk.underline.green.bold(stdout)}`));

	const parentBranch = stdout.split('/')[0];
	const subBranch = stdout.split('/')[1].trim();

	const distDir = path.join(pwd, 'dist', '');
	const putDir = path.join(remoteDir, subBranch);

	if (parentBranch !== markupBranch) {
		console.log(logErr('  ERROR - ветки должны начинаться с markup/  '));
		process.exit(1);
	}

	return ssh
		.connect({
			host: 'front-end.fmake.ru',
			username: 'fmake',
			privateKey: sshPath,
		})
		.then(() => {
			const failed = [];
			const successful = [];

			ssh.putDirectory(distDir, putDir, {
				recursive: true,
				concurrency: 1,
				tick: (localPath, remotePath, error) => {
					if (error) {
						failed.push(localPath);
					}
					else {
						successful.push(localPath);
					}
				},
			}).then(status => {
				console.log(
					'directory transfer was',
					status
						? `${chalk.green.bold('successful')} - all hail the king ${chalk.hex('#FF69B4').bold('<3')}`
						: chalk.red.bold('unsuccessful')
					// status ? chalk.green.bold('successful') + ' send <3 to @eviktorov' : chalk.red.bold('unsuccessful')
				);
				// console.log(chalk.red('failed transfers \n'), failed.join('\n'));
				// console.log(chalk.green('successful transfers \n'), successful.join('\n'));

				ssh.dispose();

				const siteUrl = `http://front-end.fmake.ru/${PROJECT_DIRECTORY}/${subBranch}/`;

				console.log(chalk.blue.bold(`\n\n\nКонтур: ${siteUrl}\n\nВетка:  ${stdout}\n\n`));
			});
		});
});
